# ~/.profile: executed by the command interpreter for login shells.
# This file is not read by bash(1), if ~/.bash_profile or ~/.bash_login
# exists.
# see /usr/share/doc/bash/examples/startup-files for examples.
# the files are located in the bash-doc package.

# the default umask is set in /etc/profile; for setting the umask
# for ssh logins, install and configure the libpam-umask package.
#umask 022

# if running bash
if [ -n "$BASH_VERSION" ]; then
    # include .bashrc if it exists
    if [ -f "$HOME/.bashrc" ]; then
	. "$HOME/.bashrc"
    fi
fi

# set PATH so it includes user's private bin if it exists
if [ -d "$HOME/bin" ] ; then
    PATH="$HOME/bin:$PATH"
fi

# Run fluidsynth, but this time as a non-interactive server
fluidsynth -is --audio-driver=alsa --gain 5 /usr/share/sounds/sf2/FluidR3_GM.sf2 &

# give it time to boot up
sleep 10

# connect the controller to fluidsynth
# Don't forget to replace these with the client numbers!
aconnect 20:0 128:0

# Give fluidsynth a nice high priority so it gets as much CPU as possible!
sudo renice -n -18 -u tdido

python src/synth/voloff.py &
