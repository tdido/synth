from RPi import GPIO
import time
import alsaaudio
from subprocess import call

m = alsaaudio.Mixer('PCM')

volint = [45,100]
volinc = 1
vol = 75
m.setvolume(vol)

def shutdown(channel):
    time.sleep(3)
    if GPIO.input(sw) == 0:
        call("sudo shutdown -h now", shell=True)

clk = 17
dt = 18
sw = 14

GPIO.setmode(GPIO.BCM)
GPIO.setup(clk, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)
GPIO.setup(dt, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)

GPIO.setup(sw, GPIO.IN, pull_up_down=GPIO.PUD_UP)
GPIO.add_event_detect(sw, GPIO.FALLING, callback=shutdown, bouncetime=30)

counter = 0
clkLastState = GPIO.input(clk)

try:

        while True:
                clkState = GPIO.input(clk)
                dtState = GPIO.input(dt)
                if clkState != clkLastState:
                        if dtState != clkState:
                            vol += volinc
                            if vol > volint[1]:
                                vol = volint[1]
                        else:
                            vol -= volinc
                            if vol < volint[0]:
                                vol = volint[0]
                        m.setvolume(vol)
                clkLastState = clkState
                time.sleep(0.01)

finally:
        GPIO.cleanup()
